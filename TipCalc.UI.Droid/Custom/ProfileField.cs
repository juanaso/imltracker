﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TipCalc.UI.Droid.Custom
{
    public class ProfileField : FrameLayout
    {
        private TextView titleTextView;
        private TextView dataTextView;
        private TypedArray typedArray;

        public string Data
        {
            get { return dataTextView.Text; }
            set { dataTextView.Text = value; }
        }

        public ProfileField(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(attrs);
            SetUpView();
        }

        public ProfileField(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(attrs);
            SetUpView();
        }

        private void Initialize(IAttributeSet attrs)
        {
            Inflate(Context, Resource.Layout.custom_user_profile_field, this);
            titleTextView = FindViewById<TextView>(Resource.Id.user_field_name);
            dataTextView = FindViewById<TextView>(Resource.Id.user_field_data);
            typedArray = Context.ObtainStyledAttributes(attrs, Resource.Styleable.ProfileField);
        }

        private void SetUpView()
        {
            titleTextView.Text = typedArray.GetString(Resource.Styleable.ProfileField_profile_field_title);
        }
    }
}