﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;
using TipCalc.Core.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;
using TipCalc.UI.Droid.Custom;

namespace TipCalc.UI.Droid.Views
{
    [Activity(Label = "MainActivity", MainLauncher = true)]
    public class MainActivity : MvxAppCompatActivity<MainViewModel>
    {
        private MvxClickableLinearLayout childrenLinearLayout;
        private ProfileField parentField;
        protected override void OnViewModelSet()
        {
            SetContentView(Resource.Layout.main_activity);
            childrenLinearLayout = FindViewById<MvxClickableLinearLayout>(Resource.Id.children_linear_layout);
            parentField = FindViewById<ProfileField>(Resource.Id.user_parent);
            childrenLinearLayout.ItemClick = ViewModel.GetUser;
            parentField.Click += (o, e) => ViewModel.GetParentUser.Execute(parentField.Data);

        }
    }
}