﻿using Newtonsoft.Json;

namespace TipCalc.Core.Data.DTO
{
    public class ChildrenDTO
    {
        [JsonProperty(PropertyName = "955039")]
        public UserDTO User1 { get; set; }

        [JsonProperty(PropertyName = "966845")]
        public UserDTO User2 { get; set; }

        [JsonProperty(PropertyName = "979559")]
        public UserDTO User3 { get; set; }

        [JsonProperty(PropertyName = "1173492")]
        public UserDTO User4 { get; set; }

        [JsonProperty(PropertyName = "1210872")]
        public UserDTO User5 { get; set; }
    }
}