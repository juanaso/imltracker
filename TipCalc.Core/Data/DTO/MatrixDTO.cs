﻿using Newtonsoft.Json;

namespace TipCalc.Core.Data.DTO
{
    public class MatrixDTO
    {
        [JsonProperty(PropertyName = "952131")]
        public UserDTO User { get; set; }

    }
}
