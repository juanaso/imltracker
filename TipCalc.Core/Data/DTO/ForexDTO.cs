﻿using Newtonsoft.Json;

namespace TipCalc.Core.Data.DTO
{
    public class ForexDTO
    {
        [JsonProperty(PropertyName = "matrix")]
        public MatrixDTO Matrix { get; set; }
    }
}
