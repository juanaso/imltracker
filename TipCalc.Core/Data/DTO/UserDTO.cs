﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TipCalc.Core.Data.Entities;
using Newtonsoft.Json;

namespace TipCalc.Core.Data.DTO
{
    public class UserDTO
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "active")]
        public string Active { get; set; }

        [JsonProperty(PropertyName = "prsv")]
        public int Prsv { get; set; }

        [JsonProperty(PropertyName = "gpsv")]
        public int Gpsv { get; set; }

        [JsonProperty(PropertyName = "rank")]
        public string Rank { get; set; }

        [JsonProperty(PropertyName = "parent")]
        public string Parent { get; set; }

        [JsonProperty(PropertyName = "billdate")]
        public string Billdate { get; set; }

        [JsonProperty(PropertyName = "catid")]
        public string Catid { get; set; }

        [JsonProperty(PropertyName = "children")]
        public ChildrenDTO Children { get; set; }

        [JsonProperty(PropertyName = "product")]
        public string Product { get; set; }
    }
}
