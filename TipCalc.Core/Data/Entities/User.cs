﻿using System.Collections.Generic;

namespace TipCalc.Core.Data.Entities
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Active { get; set; }
        public int Prsv { get; set; }
        public int Gpsv { get; set; }
        public string Rank { get; set; }
        public string Parent { get; set; }
        public string Billdate { get; set; }
        public string Catid { get; set; }
        public string Product { get; set; }
        private string Nacionality { get; set; }
        public IEnumerable<User> Children { get; set; }
    }
}
