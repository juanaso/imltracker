﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipCalc.Core.Common
{
    public static class MvxObservableCollectionExtensions
    {
        public static MvxObservableCollection<T> ToMvxCollection<T>(this IEnumerable<T> list)
            => list != null ?
            new MvxObservableCollection<T>(list) :
            new MvxObservableCollection<T>();
    }
}
