﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TipCalc.Core.Data.Entities;

namespace TipCalc.Core.Managers.Main
{
    interface IMainManager
    {
        Task<User> GetMainData(string endpoint);
    }
}
