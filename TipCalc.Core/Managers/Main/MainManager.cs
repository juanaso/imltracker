﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TipCalc.Core.Data.Entities;
using TipCalc.Core.Mappers;
using TipCalc.Core.Services.REST.Clients.Main;

namespace TipCalc.Core.Managers.Main
{
    public class MainManager : IMainManager
    {
        private IMainServiceClient mainServiceClient;
        private UserMapper mapper;
        public Task<User> GetMainData(string endpoint)
        {
            mainServiceClient = new MainServiceClient();
            mapper = new UserMapper();
            var response = mainServiceClient.GetMain(endpoint);
            var user = mapper.MapUserAux(response);
            return user;
        }
    }
}
