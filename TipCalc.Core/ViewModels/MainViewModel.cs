﻿using MvvmCross.Core.ViewModels;
using TipCalc.Core.Data.Entities;
using TipCalc.Core.Managers.Main;
using TipCalc.Core.Common;

namespace TipCalc.Core.ViewModels
{

    public class MainViewModel : MvxViewModel
    {
        private string _message;
        private User _user;
        private IMainManager mainManager;
        public IMvxCommand<User> GetUser { get; set; }
        public IMvxCommand<string> GetParentUser { get; set; }

        public override void Start()
        {
            mainManager = new MainManager();
            InitializeCommands();
            SetUpData("");
            base.Start();
        }

        public User user
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged(() => user);
                RaisePropertyChanged(() => children);
            }
        }

       public MvxObservableCollection<User> children
        {
            get
            {
                return user.Children.ToMvxCollection();
            }
        }

        private async void InitializeCommands()
        {
            GetUser = new MvxCommand<User>(DoGetUser);
            GetParentUser = new MvxCommand<string>(SetUpData);
        }

        private void DoGetUser(User user)
        {
            SetUpData(user.Id);
        }

        private async void SetUpData(string id)
        {
            user = await mainManager.GetMainData(id);
        }
    }
}
