﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TipCalc.Core.Data.DTO;
using TipCalc.Core.Data.Entities;
using TipCalc.Core.Mappers.Deserializer;

namespace TipCalc.Core.Mappers
{
    public class UserMapper
    {
        IUserDeserializer userDezerializer;
        public UserMapper()
        {
            userDezerializer = new UserDeserializer();
        }
        
        public async Task<User> MapUserAux(HttpResponseMessage response)
        {

            string a = await response.Content.ReadAsStringAsync();
            var jsonObject = JToken.Parse(a) as JObject;
            var matrix = jsonObject.First;
            var firstChild = matrix.First as JObject;
            IList<string> keys = firstChild.Properties().Select(p => p.Name).ToList();
            var h = JObject.Parse(firstChild.ToString())[keys[0]];

            return  userDezerializer.DoUserDeserializer(h as JObject);
        }
    }
}
