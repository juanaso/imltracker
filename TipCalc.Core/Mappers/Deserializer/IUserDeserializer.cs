﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TipCalc.Core.Data.Entities;

namespace TipCalc.Core.Mappers.Deserializer
{
    interface IUserDeserializer
    {
        User DoUserDeserializer(JObject jsonObject);
    }
}
