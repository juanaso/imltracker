﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TipCalc.Core.Data.DTO;
using TipCalc.Core.Data.Entities;

namespace TipCalc.Core.Mappers.Deserializer
{
    public class UserDeserializer : IUserDeserializer
    {
        public User DoUserDeserializer(JObject jsonObject)
        {
            User user =  makeUser(jsonObject,"");
            return user;
        }

        private List<User> getChildren(JObject jChildren)
        {
            List<User> children = new List<User>();
            var keys = getKeys(jChildren);
            foreach (var key in keys)
            {
                var rootChild = jChildren[key] as JObject;
                children.Add(makeUser(rootChild, key));
            }
            return children;
        }

    private User makeUser(JObject jsonObject,string Id)
        {
            User user = new User();
          
            user.Id = (string)jsonObject["id"];
            user.Name = (string)jsonObject["name"];
            user.Active = (string)jsonObject["active"];
            user.Prsv = (int)jsonObject["prsv"];
            user.Gpsv = (int)jsonObject["gpsv"];
            user.Rank = (string)jsonObject["rank"];
            user.Parent = (string)jsonObject["parent"];
            user.Billdate = (string)jsonObject["billdate"];
            user.Catid = (string)jsonObject["catid"];
            user.Product = (string)jsonObject["product"];
            user.Id = user.Id ?? Id;
            JObject jChildren = new JObject();

            try
            {
                 jChildren = (JObject)jsonObject["children"];
            }
            catch (Exception e)
            {

            }
            if (jChildren?.Count > 0)
            {
                List<User> children = getChildren(jChildren);
                user.Children = children;
            }
            return user;
        }

        private IList<string> getKeys(JObject jsonObject)
        {
            IList<string> keys = jsonObject.Properties().Select(p => p.Name).ToList();
            return keys;
        }
    }
}
