﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TipCalc.Core.Services.REST
{
    public class BaseService
    {
        
        public HttpResponseMessage DoGetUsers(string endpoint)
        {
            var httpClient = new HttpClient();
            //endpoint == "" ?? endpoint = RestResources.DummyURI : endpoint = RestResources.URI + endpoint;

            if (endpoint == "")
            {
                endpoint = RestResources.DummyURI;
            }else
            {
                endpoint = RestResources.URI + endpoint;
            }

            return httpClient.GetAsync(endpoint).Result;
        }
    }
}
