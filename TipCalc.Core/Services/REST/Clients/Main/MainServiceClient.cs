﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TipCalc.Core.Services.REST.Clients.Main
{
    public class MainServiceClient : BaseService, IMainServiceClient
    {
        public HttpResponseMessage GetMain(string endpoint)
        {
            var response = DoGetUsers(endpoint);
            return response;
        }
    }
}
