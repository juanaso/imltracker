﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TipCalc.Core.Services.REST.Clients.Main
{
    interface IMainServiceClient
    {
        HttpResponseMessage GetMain(string endpoint);
    }
}
